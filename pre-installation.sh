cryptsetup luksFormat -c aes-xts-plain64 -s 512 -h sha512 -y $2
cryptsetup luksOpen  $2 root

mkfs.ext4 $1
mkfs.ext4 /dev/mapper/root  #its root because you used luksOpen root

mount -t ext4 /dev/mapper/root /mnt
mkdir -p /mnt/boot
mount -t ext4 $1 /mnt/boot

pacstrap /mnt base base-devel

genfstab -U -p /mnt >> /mnt/etc/fstab

cp -R ../dummy-arch-installation-helper-scripts /mnt/installation
arch-chroot /mnt /installation/post-installation.sh $1 $2 $3
