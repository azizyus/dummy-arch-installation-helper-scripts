pacman -Sy linux linux-firmware linux-lts linux-lts-headers openssh grub-bios wpa_supplicant wireless_tools dialog git net-tools networkmanager --noconfirm



sed -i '/^HOOKS=/s/block/block keymap encrypt/' /etc/mkinitcpio.conf

# capital P means all of kernels will be built for /boot

#mkinitcpio -P


# we need to build them after installation of linux-lts otherwise lts kernel wont be have encrypt hook and you wont able to boot from it because of encryption
mkinitcpio -P

#grub-install --target=i386-pc --recheck /dev/sdb

# sdb1 is /boot and sdb2 is your /

# add cryptdevice="cryptdevice:/dev/sdb2:root" to /etc/default/grub

# add encrypt hook to /etc/mkinitcpio.conf HOOK="_OTHER_THINGS_ block encrypt _OTHER_THINGS_"


SDB2="$(find -L /dev/disk/by-uuid -samefile $2 | cut -d/ -f5)"
sed -i '/GRUB_CMDLINE_LINUX=/s/"$/cryptdevice=UUID='$SDB2':root&/' /etc/default/grub
echo $SDB2 is your encrypted partition


grub-install --target=i386-pc --recheck $3
grub-mkconfig --output /boot/grub/grub.cfg


echo "REBOOT YOUR COMPUTER AND RUN installation.sh"
