#update package db
pacman -Sy


#ntfs 
pacman -S --noconfirm ntfs-3g 

#xorg
pacman -S --noconfirm xorg libva-intel-driver mesa
pacman -S --noconfirm xf86-video-intel xf86-input-synaptics

#i3
pacman -S --noconfirm i3 i3-gaps i3status i3lock dmenu compton

#greeter and login
pacman -S --noconfirm lightdm lightdm-gtk-greeter




#xfce
pacman -S --noconfirm xfce4

pacman -S --noconfirm flameshot volumeicon cmake automake make fmt firefox vlc wget xterm qbittorrent macchanger net-tools networkmanager nautilus udisks2 nmap gvfs xfce4-goodies sysstat acpi dsniff


#pulse
pacman -S --noconfirm pulseaudio pavucontrol alsa-firmware alsa-utils


#inject greeter to config file
echo "greeter-session=lightdm-gtk-greeter" >> /etc/lightdm/lightdm.conf



#systemctl
systemctl enable lightdm
